import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 12, name: 'Captain America' },
      { id: 13, name: 'Iron Man' },
      { id: 14, name: 'Hawkeye ' },
      { id: 15, name: 'Hulk' },
      { id: 16, name: 'Black Widow' },
      { id: 17, name: 'Doctor Strange' },
      { id: 18, name: 'Winter Soldier' },
      { id: 19, name: 'Black Panther' },
      { id: 20, name: 'Rocket' }
    ];
    return {heroes};
  }


  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
}